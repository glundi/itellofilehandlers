# README #

This project uses [Gradle](https://gradle.org/) as a build tool. Use `gradle test` to run the tests. Packaging the api into a jar is not supported yet. JavaDoc comments are somewhat sparse atm.

### Which files are the most interesting? ###

* `events.FileHandlerDispatcher` contains the logic for reading files from the disc and dispatching a handler depending on the file format. `handleFile` is the method that should be called for each new file.
* `events.StandardFileHandler` is a FileHandlerDispatcher that has been configured to support the two example formats.
* `fileformats.PaymentFileHandler` contains the logic that is common to handling both example formats.
* `fileformats.Inbetalningstjansten` contains the logic for decoding a Inbetalningstjansten file.
* `fileformats.Betalningsservice` contains the logic for decoding a Betalningsservice file.
* `fileformats.ParsingUtility` contains the logic for parsing common types from padded substrings.