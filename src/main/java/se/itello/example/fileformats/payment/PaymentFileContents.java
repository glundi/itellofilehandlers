package se.itello.example.fileformats.payment;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import se.itello.example.fileformats.exceptions.FileFormatVerificationException;
import se.itello.example.payments.PaymentReceiver;

class PaymentFileContents {
	private String accountNumber, currency;
	private int numberPayments = 0;
	private BigDecimal expectedSum;
	private List<Payment> payments = new LinkedList<Payment>();
	private Date date;
	
	void verify(String path) {
		// First verify that the file looks good.
		BigDecimal calculatedSum = new BigDecimal(0.0);
		for(Payment p : payments) {
			calculatedSum = calculatedSum.add(p.getAmount());
		}
		
		if (! calculatedSum.equals(expectedSum)) { 
			throw new FileFormatVerificationException(String.format("Expected payments to sum up to %s, but they sum up to %s", expectedSum.toString(), calculatedSum.toString()), path);
		}
		
		if (payments.size() != numberPayments) { 
			throw new FileFormatVerificationException(String.format("Expected %d payments but file contained %d payments", payments.size(), numberPayments), path);
		}
	}
	
	void sendToReceiver(PaymentReceiver recv) {
		recv.startPaymentBundle(accountNumber, date, currency);
		for(Payment p : payments) {
			recv.payment(p.getAmount(), p.getReference());
		}
		recv.endPaymentBundle();
	}
	
	void addPayment(Payment payment) {
		this.payments.add(payment);
	}
	
	
	// Only getters and setters here
	String getAccountNumber() {
		return accountNumber;
	}
	
	void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	String getCurrency() {
		return currency;
	}
	
	void setCurrency(String currency) {
		this.currency = currency;
	}
	
	int getNumberPayments() {
		return numberPayments;
	}
	
	void setNumberPayments(int numberPayments) {
		this.numberPayments = numberPayments;
	}
	
	BigDecimal getExpectedSum() {
		return expectedSum;
	}
	
	void setExpectedSum(BigDecimal expectedSum) {
		this.expectedSum = expectedSum;
	}
	
	List<Payment> getPayments() {
		return payments;
	}
	
	Date getDate() {
		return date;
	}
	
	void setDate(Date date) {
		this.date = date;
	}
}
