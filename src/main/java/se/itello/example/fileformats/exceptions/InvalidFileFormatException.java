package se.itello.example.fileformats.exceptions;

@SuppressWarnings("serial")
public class InvalidFileFormatException extends RuntimeException {
	String path, msg;
	int line;
	
	public InvalidFileFormatException(String msg, String path, int line) {
		this(msg);
		complete(path, line);
	}
	
	public InvalidFileFormatException(String msg) {
		this.msg = msg;
	}
	
	public void complete(String path, int line) {
		this.path = path;
		this.line = line;
	}

	
	public String toString() {
		return String.format("InvalidFileFormatExpcetion when parsing %s: %s at line %d.", path, msg, line);
	}
}
