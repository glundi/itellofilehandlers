package se.itello.example.events;

import static java.nio.charset.StandardCharsets.ISO_8859_1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import se.itello.example.fileformats.FileFormatHandler;

public class FileHandlerDispatcher {
	Map<String, FileFormatHandler> handlers = new HashMap<String, FileFormatHandler>();
	
	/**
     * Dispatches a {@code FileFormatHandler} that has been registered to a suffix of {@code path}.
     * @param path The path to the new file.
     * @return true if a matching handler was found and dispatched. Otherwise false.
	 * @throws IOException if an I/O error occurs when trying to read the file at {@code path}.
     */
	public boolean handleFile(String path) throws IOException {
		return dispatch(path, readAllLines(path));
	}
	
	boolean dispatch(String path, String[] lines) {
		for(String suffix : handlers.keySet()) {
			if (path.endsWith(suffix)) {
				handlers.get(suffix).handleNewFile(path, lines);
				return true;
			}
		}
		return false;
	}
	
	/**
     * Registers a {@code FileFormatHandler} to a {@code suffix} so that it can be dispatched by {@code handleFile}.
     * It is up to the user to ensure that registered handlers aren't suffixes of each other.
     * @param suffix The suffix to which we should register the {@code handler}.
     * @param handler The {@code FileFormatHandler} that should be associated with the {@code suffix}.
     */
	public void registerHandler(String suffix, FileFormatHandler handler) {
		// TODO: Add a check to ensure that the new suffix isn't a suffix of an existing suffix, or vice versa.
		handlers.put(suffix, handler);
	}
	
	private String[] readAllLines(String path) throws IOException {
		return Files.lines(Paths.get(path), ISO_8859_1).toArray(String[]::new);
	}
}
