package se.itello.example.fileformats.payment;

import java.math.BigDecimal;

public class Payment {
	private final BigDecimal amount;
	private final String reference;
	
	public Payment(BigDecimal amount, String reference) {
		this.amount = amount;
		this.reference = reference;
	}
	
	public Payment(String amount, String reference) {
		this.amount = new BigDecimal(amount);
		this.reference = reference;
	}
	
	public boolean equals(Object other) {
		if (other == null) { return false; }
		if (!(other instanceof Payment)) { return false; }
		
		Payment otherP = (Payment) other;
		return getAmount().equals(otherP.getAmount()) && getReference().equals(otherP.getReference());
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getReference() {
		return reference;
	}
}