package se.itello.example.fileformats.utility;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import se.itello.example.fileformats.exceptions.InvalidFileFormatException;

public class ParsingUtility {
	
	public static enum Padding{
		LEFT_PADDED, RIGHT_PADDED;
	}
	
	private static String leftFilled(String src, int left, int right) {
		while(src.charAt(left) == ' ' && right > left) {
			left++;
		}
		
		return src.substring(left, right + 1);
	}
	
	private static String rightFilled(String src, int left, int right) {
		while(src.charAt(right) == ' ' && right > left) {
			right--;
		}
		
		return src.substring(left, right + 1);
	}
	
	public static String parseString(String src, int left, int right, Padding padding) {
		// Since all the other parsing methods call this one, here is where we do the
		// conversion from 1-indexed to 0-indexed strings.
		if (padding == Padding.LEFT_PADDED) {
			return leftFilled(src, left-1, right-1);
		} else {
			return rightFilled(src, left-1, right-1);
		}
	}
	
	public static int parseInt(String src, int left, int right, Padding padding) {
		String subs = parseString(src, left, right, padding);
		try {
			return Integer.parseInt(subs);
		} catch(NumberFormatException e) {
			throw new InvalidFileFormatException(String.format("Invalid Integer (%s)", subs));
		}
	}
	
	public static BigDecimal parseDecimal(String src, int left, int right, Padding padding) {
		String subs = parseString(src, left, right, padding);
		try {
			// TODO: Handle different locales for decimal numbers.
			return new BigDecimal(subs.replaceAll(",", "."));
		} catch(NumberFormatException e) {
			throw new InvalidFileFormatException(String.format("Invalid Decimal (%s)", subs));
		}
	}
	
	public static Date parseDate(String src, int left, int right, Padding padding) {
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		String subs = parseString(src, left, right, padding);
		try {
			return format.parse(subs);
		} catch (ParseException e) {
			throw new InvalidFileFormatException(String.format("Invalid Date (%s)", subs));
		}
	}
}
