package se.itello.example.events;

import java.util.LinkedList;
import java.util.List;

import se.itello.example.fileformats.FileFormatHandler;

class MockHandler implements FileFormatHandler{

	private List<String> recordedPaths = new LinkedList<String>();
	
	public void handleNewFile(String path, String[] lines) {
		recordedPaths.add(path);
	}

	List<String> getRecordedPaths() {
		return recordedPaths;
	}
}
