package se.itello.example.fileformats;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import se.itello.example.events.FileHandlerDispatcher;
import se.itello.example.events.StandardFileHandler;
import se.itello.example.fileformats.exceptions.FileFormatVerificationException;
import se.itello.example.fileformats.payment.Betalningsservice;
import se.itello.example.fileformats.payment.Inbetalningstjansten;
import se.itello.example.fileformats.payment.Payment;

public class FileFormatTest {
	
	@Test
	public void testBetalningsserviceExample() {
		MockReceiver recv = new MockReceiver();
		StandardFileHandler dispatcher = new StandardFileHandler(recv);
		
		try {
			dispatcher.handleFile("resources/Exempelfil_betalningsservice.txt");
			fail("Betalningsservice example one should not verify!");
		} catch (FileFormatVerificationException e) {
			if (! (e.msg.contains("Expected payments to sum up to"))) {
				fail("Betaltningsservice example one should fail with an error message about the sum!");
			}
		} catch (Exception e) {
			fail("Betaltningsservice example one should not fail with an exception other than a FileFormatVerificationException");
		}
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testBetalningsserviceExample2() {
		MockReceiver recv = new MockReceiver();
		StandardFileHandler dispatcher = new StandardFileHandler(recv);
		try {
			dispatcher.handleFile("resources/Exempelfil2_betalningsservice.txt");
		} catch(Exception e) {
			fail("Betalningsservice example two should not throw an exeption.");
		}
		assertEquals("Currency is correctly parsed", "SEK", recv.currency);
		// Note that years start counting at 1900
		assertEquals("Payment year is correctly parsed", 2011 - 1900, recv.paymentDate.getYear());
		// Note that the months starts counting at 0
		assertEquals("Payment month is correctly parsed", 2, recv.paymentDate.getMonth());
		assertEquals("Payment day is correctly parsed", 15, recv.paymentDate.getDate());
		assertEquals("Account number is correctly parsed", "5555 5555555555",  recv.accountNumber);
		
		List<Payment> expectedPayments = new LinkedList<Payment>();
		expectedPayments.add(new Payment("3000", "1234567890"));
		expectedPayments.add(new Payment("1000", "2345678901"));
		expectedPayments.add(new Payment("300.10", "3456789012"));
		expectedPayments.add(new Payment("400.07", "4567890123"));
		assertEquals("Payments are correctly parsed", expectedPayments, recv.payments);
		assertTrue("endPaymentBundle was called", recv.hasBeenEnded);
	}
	
	@Test
	public void testInbetalningstjanstenExample() {
		FileHandlerDispatcher dispatcher = new FileHandlerDispatcher();
		MockReceiver recv = new MockReceiver();
		MockReceiver emptyRecv = new MockReceiver();
		dispatcher.registerHandler("_betalningsservice.txt", new Betalningsservice(emptyRecv));
		dispatcher.registerHandler("_inbetalningstjansten.txt", new Inbetalningstjansten(recv));
		try {
			dispatcher.handleFile("resources/Exempelfil_inbetalningstjansten.txt");
		} catch(Exception e) {
			fail("Inbetalningstjansten exampe should not throw an exeption.");
		}
		
		
		assertNull("The betalningsservice handler was never used.", emptyRecv.currency);
		assertEquals("Currency is correctly parsed", recv.currency, "SEK");
		assertEquals("Account number is correctly parsed", "1234 1234567897",  recv.accountNumber);
		
		List<Payment> expectedPayments = new LinkedList<Payment>();
		expectedPayments.add(new Payment("4000.00", "9876543210"));
		expectedPayments.add(new Payment("1000.00", "9876543210"));
		expectedPayments.add(new Payment("10300.00", "9876543210"));
		assertEquals("Payments are correctly parsed", expectedPayments, recv.payments);
		assertTrue("endPaymentBundle was called", recv.hasBeenEnded);
	}

}
