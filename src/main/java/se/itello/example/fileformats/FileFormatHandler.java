package se.itello.example.fileformats;

public interface FileFormatHandler {
	public void handleNewFile(String path, String[] lines);
}
