package se.itello.example.fileformats;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import se.itello.example.fileformats.payment.Payment;
import se.itello.example.payments.PaymentReceiver;

class MockReceiver implements PaymentReceiver{
	
	String accountNumber, currency;
	Date paymentDate;
	List<Payment> payments = new LinkedList<Payment>();
	boolean hasBeenEnded = false;

	@Override
	public void startPaymentBundle(String accountNumber, Date paymentDate, String currency) {
		this.accountNumber = accountNumber;
		this.paymentDate = paymentDate;
		this.currency = currency;
	}

	@Override
	public void payment(BigDecimal amount, String reference) {
		// AbstractMap.SimpleEntry is used as a pair
		payments.add(new Payment(amount, reference));
	}

	@Override
	public void endPaymentBundle() {
		hasBeenEnded = true;
	}

}
