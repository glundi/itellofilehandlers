package se.itello.example.fileformats.payment;

import java.math.BigDecimal;
import java.util.Date;

import se.itello.example.payments.PaymentReceiver;

public class Inbetalningstjansten extends PaymentFileHandler{
	
	public Inbetalningstjansten(PaymentReceiver recv) {
		this.recv = recv;
		reset();
	}
	
	protected void handleOpeningPost(String line) {
		expect("00", line, 1, 2);
		int clearing = parseInt(line, 11, 14);
		int number = parseInt(line, 15, 24);
		
		//It is not specified how to compose the clearing number and account number. However, 
		// the other format composes them using a space so well use that here too.
		contents.setAccountNumber(Integer.toString(clearing) + " " + Integer.toString(number));
	}
	
	protected void handlePaymentPost(String line) {
		expect("30", line, 1, 2);
		BigDecimal amountNoDecimals = parseDecimal(line, 3, 22);
		// Amount is known to be specified with two decimals
		BigDecimal amount = amountNoDecimals.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
		String reference = parseString(line, 41, 65);
		
		contents.addPayment(new Payment(amount, reference));
	}
	
	protected void handleClosingPost(String line) {
		expect("99", line, 1, 2);
		BigDecimal sumNoDecimals = parseDecimal(line, 3, 22);
		contents.setExpectedSum(sumNoDecimals.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
		contents.setNumberPayments(parseInt(line, 31, 38));
	}
	
	@Override
	protected void reset() {
		contents = new PaymentFileContents();
		// OBS! What to use as payment date has not been specified for this format.
		// Today's date seems sensible but we ought to ask someone about it.
		contents.setDate(new Date());
		contents.setCurrency("SEK");
	}
}
