package se.itello.example.fileformats;

import se.itello.example.fileformats.exceptions.InvalidFileFormatException;

abstract public class FirstMiddleLastFileHandler implements FileFormatHandler{
	
	public void handleNewFile(String path, String[] lines){
		int i = 0;
		// Process all lines
		try {
			handleOpeningPost(lines[0]);
			for(i = 1; i < lines.length - 1; ++i) {
				handlePaymentPost(lines[i]);
			}
			handleClosingPost(lines[lines.length - 1]);
		} catch(InvalidFileFormatException e) {
			e.complete(path, i);
			throw e;
		}
		
		// Verify and process the contents
		handleContents(path);
	}
	
	protected abstract void handleOpeningPost(String line);
	protected abstract void handlePaymentPost(String line);
	protected abstract void handleClosingPost(String line);
	protected abstract void handleContents(String path);
}
