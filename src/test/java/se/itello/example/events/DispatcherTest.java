package se.itello.example.events;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.Test;

public class DispatcherTest {

	@Test
	public void testHandlerSelection() {
		FileHandlerDispatcher dispatcher = new FileHandlerDispatcher();
		MockHandler handler = new MockHandler();
		ReversedMockHandler revHandler = new ReversedMockHandler();
		
		dispatcher.registerHandler("foo", handler);
		dispatcher.registerHandler("bar", revHandler);
		
		
		// We will test that the dispatcher separates "foo"s and "bar"s.
		String[] paths = {"1foo", "2foo", "3bar", "4foo", "5foo", "6bar"};
		for(String path : paths) {
			try {
				dispatcher.dispatch(path, new String[0]);
			} catch(Exception e) {
				// The mock version never throws an exception
			}
		}
		
		for(String path : handler.getRecordedPaths()) {
			assertThat(path, containsString("foo"));
		}
		
		for(String path : revHandler.getRecordedPaths()) {
			assertThat(path, containsString("rab"));
		}
	}

}
