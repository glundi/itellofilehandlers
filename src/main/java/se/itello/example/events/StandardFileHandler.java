package se.itello.example.events;

import se.itello.example.fileformats.payment.Betalningsservice;
import se.itello.example.fileformats.payment.Inbetalningstjansten;
import se.itello.example.payments.PaymentReceiver;

public class StandardFileHandler extends FileHandlerDispatcher {
	
	public StandardFileHandler(PaymentReceiver recv) {
		super();
		registerHandler("_betalningsservice.txt", new Betalningsservice(recv));
		registerHandler("_inbetalningstjansten.txt", new Inbetalningstjansten(recv));
	}

}
