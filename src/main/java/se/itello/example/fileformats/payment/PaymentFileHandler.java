package se.itello.example.fileformats.payment;

import java.math.BigDecimal;
import java.util.Date;

import se.itello.example.fileformats.FirstMiddleLastFileHandler;
import se.itello.example.fileformats.exceptions.InvalidFileFormatException;
import se.itello.example.fileformats.utility.ParsingUtility;
import se.itello.example.payments.PaymentReceiver;

abstract class PaymentFileHandler extends FirstMiddleLastFileHandler{
	protected PaymentReceiver recv;
	protected PaymentFileContents contents = new PaymentFileContents();
	
	@Override
	protected void handleContents(String path) {
		try {
			contents.verify(path);
			contents.sendToReceiver(recv);
		} finally {
			reset();
		}
	}
	
	protected String parseString(String line, int left, int right) {
		return ParsingUtility.parseString(line, left, right, ParsingUtility.Padding.RIGHT_PADDED);
	}
	
	protected int parseInt(String line, int left, int right) {
		return ParsingUtility.parseInt(line, left, right, ParsingUtility.Padding.LEFT_PADDED);
	}
	
	protected BigDecimal parseDecimal(String line, int left, int right) {
		return ParsingUtility.parseDecimal(line, left, right, ParsingUtility.Padding.RIGHT_PADDED);
	}
	
	protected Date parseDate(String line, int left, int right) {
		return ParsingUtility.parseDate(line, left, right, ParsingUtility.Padding.LEFT_PADDED);
	}
	
	protected void expect(String expected, String line, int left, int right) {
		String actual = parseString(line, left, right);
		if (!actual.equals(expected)) {
			throw new InvalidFileFormatException(String.format("Expected constant %s, but got %s", expected, actual));
		}
	}
	
	/**
	 * Should replace the contents field with a new instance and possibly fill in some default parameters.
	 */
	protected void reset() {
		contents = new PaymentFileContents();
	}
}
