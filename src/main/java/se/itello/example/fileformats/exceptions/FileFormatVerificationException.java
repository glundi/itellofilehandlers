package se.itello.example.fileformats.exceptions;

@SuppressWarnings("serial")
public class FileFormatVerificationException extends RuntimeException {
	public String path, msg;
	
	public FileFormatVerificationException(String msg, String path) {
		this.msg = msg;
		this.path = path;
	}
	
	public String toString() {
		return String.format("FileFormatVerificationException when parsing %s: %s.", path, msg);
	}
}
