package se.itello.example.fileformats.payment;

import java.math.BigDecimal;
import java.util.Date;

import se.itello.example.fileformats.utility.ParsingUtility;
import se.itello.example.payments.PaymentReceiver;

public class Betalningsservice extends PaymentFileHandler{
	
	public Betalningsservice(PaymentReceiver recv) {
		this.recv = recv;
		this.contents = new PaymentFileContents();
	}
	
	protected void handleOpeningPost(String line) {
		expect("O", line, 1, 1);
		contents.setAccountNumber(parseString(line, 2, 16));
		contents.setExpectedSum(parseDecimal(line, 17, 30));
		contents.setNumberPayments(parseInt(line, 31, 40));
		contents.setDate(parseDate(line, 41, 48));
		contents.setCurrency(parseString(line, 49, 51));
	}
	
	protected void handlePaymentPost(String line) {
		expect("B", line, 1, 1);
		BigDecimal amount = parseDecimal(line, 2, 15);
		String reference = parseString(line, 16, 50);
		
		contents.addPayment(new Payment(amount, reference));
	}
	
	// The Betalningsservice format does not have a separate closing post.
	protected void handleClosingPost(String line) {
		handlePaymentPost(line);
	}

	// We're only changing the padding direction.
	@Override
	protected BigDecimal parseDecimal(String line, int left, int right) {
		return ParsingUtility.parseDecimal(line, left, right, ParsingUtility.Padding.LEFT_PADDED);
	}
}
